import React from 'react';

const TotalSpent = props => {
	return (
		<div className="totalSpent">Total spent: <span>{props.totalSpent} KGS</span></div>
	)
};

export default TotalSpent;