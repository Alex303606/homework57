import React from 'react';
import DiagramItem from './DiagramItem/DiagramItem';
import Legend from "./Legend/Legend";
import Wrapper from "../../../hoc/Wrapper";

const Diagram = props => {
	const categoryInfo = props.costs.reduce((acc, cost) => {
		switch (cost.category) {
			case 'Other':
				acc.other += parseInt(cost.price, 0);
				break;
			case 'Entertainment':
				acc.entertainment += parseInt(cost.price, 0);
				break;
			case 'Car':
				acc.car += parseInt(cost.price, 0);
				break;
			case 'Food':
				acc.food += parseInt(cost.price, 0);
				break;
			default:
				return null;
		}
		return acc;
	}, {other: 0, entertainment: 0, car: 0, food: 0});
	let totalSum = Object.values(categoryInfo).reduce((acc, price) => acc += price);
	let elementsStyle = Object.keys(categoryInfo).map(key => {
		if (totalSum !== 0) {return {[key]: {width: (categoryInfo[key] / totalSum * 100) + '%'}};
		} else {return {[key]: {width: 0}};}
	});
	return (
		<Wrapper>
			<div className="Diagram">
				{
					elementsStyle.map(elem => {
						return <DiagramItem key={Object.keys(elem)[0]} style={elem[Object.keys(elem)[0]]} name={Object.keys(elem)[0]}/>
					})
				}
			</div>
			<Legend items={elementsStyle}/>
		</Wrapper>
		
	)
};

export default Diagram;