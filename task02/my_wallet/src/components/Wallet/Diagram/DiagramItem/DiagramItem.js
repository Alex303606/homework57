import React from 'react';

const DiagramItem = props => {
	return <span style={props.style} className={props.name}></span>
};

export default DiagramItem;