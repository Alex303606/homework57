import React from 'react';

const LegendItem = props => {
	return <li><span className={['icon',props.name].join(' ')}></span>{props.name}</li>
};

export default LegendItem;