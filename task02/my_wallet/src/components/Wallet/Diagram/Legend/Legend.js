import React from 'react';
import LegendItem from "./LegendItem/LegendItem";

const Legend = props => {
	return (
		<ul className="Legend">
			{
				props.items.map(item => {
					return <LegendItem key={Object.keys(item)[0]} name={Object.keys(item)[0]}/>
				})
			}
		</ul>
	)
};

export default Legend;