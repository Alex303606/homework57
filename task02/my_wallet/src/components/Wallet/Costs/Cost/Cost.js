import React from 'react';

const Cost = props => {
	return (
		<li>
			<span className="category">{props.category}</span>
			<span className="name">{props.name}</span>
			<span className="price">{props.price} KGS</span>
			<button onClick={() => props.deleteCost(props.id)} className="delete"><i className="fa fa-times" aria-hidden="true"></i></button>
		</li>
	)
};

export default Cost;