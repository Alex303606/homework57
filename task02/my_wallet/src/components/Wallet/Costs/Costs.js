import React from 'react';
import Cost from "./Cost/Cost";

const Costs = props => {
	return (
		<ul>
			{
				props.costs.map(cost => {
					return <Cost
						key={cost.id}
						name={cost.name}
						price={cost.price}
						deleteCost={props.deleteCost}
						id={cost.id}
						category={cost.category}
					/>;
				})
			}
		</ul>
	)
};

export default Costs;