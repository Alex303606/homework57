import React from 'react'
import Costs from "./Costs/Costs";
import TotalSpent from "./TotalSpent/TotalSpent";

const Wallet = props => {
	return (
		<div className="content">
			<Costs costs={props.costs} deleteCost={props.deleteCost}/>
			<TotalSpent totalSpent={props.totalSpent}/>
		</div>
	)
};

export default Wallet;