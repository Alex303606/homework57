import React from 'react';

const Header = props => {
	return (
		<header>
			<select defaultValue="Other" onChange={props.changeCategory}  className="select">
				<option value="Other">Other</option>
				<option value="Entertainment">Entertainment</option>
				<option value="Car">Car</option>
				<option value="Food">Food</option>
			</select>
			<input onClick={props.resetName} onChange={props.changeName} className="name" type="text" value={props.nameValue}/>
			<input onClick={props.resetCost} onChange={props.changeCost} className="cost" type="text" value={props.costValue}/>
			<span className="currency">KGS</span>
			<button onClick={props.addCost} className="add">Add</button>
		</header>
	)
};

export default Header;