import React, {Component} from 'react'
import './MyWallet.css';
import Diagram from "../../components/Wallet/Diagram/Diagram";
import Wallet from "../../components/Wallet/Wallet";
import Header from "../../components/Wallet/Header/Header";


class MyWallet extends Component {
	
	state = {
		costs: [],
		nameValue: 'Item name',
		costValue: '0',
		category: 'Other',
		totalSpent: 0
	};
	
	changeName = (event) => {
		let nameValue = this.state.nameValue;
		nameValue = event.target.value;
		this.setState({nameValue});
	};
	
	changeCost = (event) => {
		let costValue = this.state.costValue;
		costValue = event.target.value.replace(/[^0-9]/g,'');
		this.setState({costValue});
	};
	
	addCost = () => {
		const costs = [...this.state.costs];
		let totalSpent = this.state.totalSpent;
		let nameValue = this.state.nameValue;
		let costValue = this.state.costValue;
		let category = this.state.category;
		let random = nameValue + costValue + Math.random();
		let newCost = {category: category,name: nameValue, price: costValue, id: random};
		costs.push(newCost);
		totalSpent += parseInt(costValue, 0);
		this.setState({costs, totalSpent});
	};
	
	deleteCost = (id) => {
		const costs = [...this.state.costs];
		const index = costs.findIndex(p => p.id === id);
		let totalSpent = this.state.totalSpent;
		totalSpent -= costs[index].price;
		costs.splice(index, 1);
		this.setState({costs, totalSpent});
	};
	
	resetName = () => {
		let nameValue = this.state.nameValue;
		nameValue = '';
		this.setState({nameValue});
	};
	
	resetCost = () => {
		let costValue = this.state.costValue;
		costValue = '';
		this.setState({costValue});
	};
	
	changeCategory = (event) => {
		let category = this.state.category;
		category = event.target.value;
		this.setState({category});
	};
	
	render() {
		return (
			<div className="MyWallet">
				<Header
					changeName={(event) => this.changeName(event)}
					changeCost={(event) => this.changeCost(event)}
					addCost={() => this.addCost()}
					nameValue={this.state.nameValue}
					costValue={this.state.costValue}
					resetName={(event) => this.resetName(event)}
					resetCost={(event) => this.resetCost(event)}
					changeCategory={(event) => this.changeCategory(event)}
				/>
				<Wallet
					costs={this.state.costs}
					totalSpent={this.state.totalSpent}
					deleteCost={this.deleteCost}
				/>
				<Diagram costs={this.state.costs}/>
			</div>
		)
	}
	
}

export default MyWallet;