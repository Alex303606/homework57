const tasks = [
	{id: 234, title: 'Create user registration API', timeSpent: 4, category: 'Backend', type: 'task'},
	{id: 235, title: 'Create user registration UI', timeSpent: 8, category: 'Frontend', type: 'task'},
	{id: 237, title: 'User sign-in via Google UI', timeSpent: 3.5, category: 'Frontend', type: 'task'},
	{id: 238, title: 'User sign-in via Google API', timeSpent: 5, category: 'Backend', type: 'task'},
	{id: 241, title: 'Fix account linking', timeSpent: 5, category: 'Backend', type: 'bug'},
	{id: 250, title: 'Fix wrong time created on new record', timeSpent: 1, category: 'Backend', type: 'bug'},
	{id: 262, title: 'Fix sign-in failed messages', timeSpent: 2, category: 'Frontend', type: 'bug'},
];
let timeSpentFrontEnd = tasks.reduce((acc, task) => {
	if (task.category === 'Frontend') acc += task.timeSpent;
	return acc;
}, 0);
let timeSpentTypeBug = tasks.reduce((acc, task) => {
	if (task.type === 'bug') acc += task.timeSpent;
	return acc;
}, 0);
let quantityTaskUI = tasks.reduce((acc, item) => {
	if (item.title.includes('UI')) acc++;
	return acc;
}, 0);
let quantityTask = tasks.reduce((acc, task) => {
	if (task.category === 'Frontend') acc.Frontend++;
	if (task.category === 'Backend') acc.Backend++;
	return acc;
}, {Frontend: 0, Backend: 0});
let timeSpentMore4Hour = tasks.filter(task => task.timeSpent > 4);
let refactorTimeSpentMore4Hour = timeSpentMore4Hour.map(task => ({title: task.title, category: task.category}));
console.log('Общее количество времени, затраченное на работу над задачами из категории "Frontend":', timeSpentFrontEnd);
console.log('Общее количество времени, затраченное на работу над задачами типа "bug":', timeSpentTypeBug);
console.log('Количество задач, имеющих в названии слово "UI":', quantityTaskUI);
console.log('Получите количество задач каждой категории в объект вида:', quantityTask);
console.log('Массив задач с затраченным временем больше 4 часов: ', refactorTimeSpentMore4Hour);
